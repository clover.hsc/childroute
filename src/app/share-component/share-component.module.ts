import { FooterComponent } from './../footer/footer.component';
import { NavigationComponent } from './../navigation/navigation.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ NavigationComponent, FooterComponent ],
  imports: [ CommonModule ],
  exports: [ NavigationComponent, FooterComponent ],
})
export class ShareComponentModule {}
