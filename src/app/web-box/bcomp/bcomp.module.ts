import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BcompRoutingModule } from './bcomp-routing.module';
import { BcompComponent } from './bcomp/bcomp.component';


@NgModule({
  declarations: [BcompComponent],
  imports: [
    CommonModule,
    BcompRoutingModule
  ]
})
export class BcompModule { }
