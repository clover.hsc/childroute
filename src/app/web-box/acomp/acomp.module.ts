import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcompRoutingModule } from './acomp-routing.module';
import { AcompComponent } from './acomp/acomp.component';


@NgModule({
  declarations: [AcompComponent],
  imports: [
    CommonModule,
    AcompRoutingModule
  ]
})
export class AcompModule { }
