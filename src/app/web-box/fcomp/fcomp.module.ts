import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FcompRoutingModule } from './fcomp-routing.module';
import { FcompComponent } from './fcomp/fcomp.component';


@NgModule({
  declarations: [FcompComponent],
  imports: [
    CommonModule,
    FcompRoutingModule
  ]
})
export class FcompModule { }
