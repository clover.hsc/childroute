import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FcompComponent } from './fcomp.component';

describe('FcompComponent', () => {
  let component: FcompComponent;
  let fixture: ComponentFixture<FcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
