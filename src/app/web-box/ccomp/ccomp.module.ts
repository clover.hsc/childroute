import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CcompRoutingModule } from './ccomp-routing.module';
import { CcompComponent } from './ccomp/ccomp.component';


@NgModule({
  declarations: [CcompComponent],
  imports: [
    CommonModule,
    CcompRoutingModule
  ]
})
export class CcompModule { }
