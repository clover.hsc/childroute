import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DcompRoutingModule } from './dcomp-routing.module';
import { DcompComponent } from './dcomp/dcomp.component';


@NgModule({
  declarations: [DcompComponent],
  imports: [
    CommonModule,
    DcompRoutingModule
  ]
})
export class DcompModule { }
