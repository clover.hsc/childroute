import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DcompComponent } from './dcomp.component';

describe('DcompComponent', () => {
  let component: DcompComponent;
  let fixture: ComponentFixture<DcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
