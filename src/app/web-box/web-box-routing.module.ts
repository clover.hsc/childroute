import { WebBoxComponent } from './web-box/web-box.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: WebBoxComponent,
    children: [
      {
        path: 'acomp',
        loadChildren: () =>
          import('./acomp/acomp.module').then((m) => m.AcompModule),
      },
      {
        path: 'bcomp',
        loadChildren: () =>
          import('./bcomp/bcomp.module').then((m) => m.BcompModule),
      },
      {
        path: 'ccomp',
        loadChildren: () =>
          import('./ccomp/ccomp.module').then((m) => m.CcompModule),
      },
      {
        path: 'dcomp',
        loadChildren: () =>
          import('./dcomp/dcomp.module').then((m) => m.DcompModule),
      },
      {
        path: 'ecomp',
        loadChildren: () =>
          import('./ecomp/ecomp.module').then((m) => m.EcompModule),
      },
      {
        path: 'fcomp',
        loadChildren: () =>
          import('./fcomp/fcomp.module').then((m) => m.FcompModule),
      },
      { path: '', redirectTo: 'acomp', pathMatch: 'full' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebBoxRoutingModule {}
