import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-web-box',
  templateUrl: './web-box.component.html',
  styleUrls: ['./web-box.component.scss'],
})
export class WebBoxComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    const test = interval(1000);
    test.subscribe((x) => console.log(x));
  }
}
