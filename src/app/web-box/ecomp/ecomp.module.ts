import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EcompRoutingModule } from './ecomp-routing.module';
import { EcompComponent } from './ecomp/ecomp.component';


@NgModule({
  declarations: [EcompComponent],
  imports: [
    CommonModule,
    EcompRoutingModule
  ]
})
export class EcompModule { }
