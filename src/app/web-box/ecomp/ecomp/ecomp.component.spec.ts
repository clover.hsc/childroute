import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcompComponent } from './ecomp.component';

describe('EcompComponent', () => {
  let component: EcompComponent;
  let fixture: ComponentFixture<EcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
