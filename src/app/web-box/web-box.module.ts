import { ShareComponentModule } from './../share-component/share-component.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebBoxRoutingModule } from './web-box-routing.module';
import { WebBoxComponent } from './web-box/web-box.component';

@NgModule({
  declarations: [ WebBoxComponent ],
  imports: [ CommonModule, WebBoxRoutingModule, ShareComponentModule ],
})
export class WebBoxModule {}
